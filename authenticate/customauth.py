import datetime
from django.contrib.auth.models import User
from .models import Token
from .utils.sqlquery import SqlQuery
from .mockapi.mocksmspanel import verify_code
from django.contrib.auth import login


class CustomAuth():
    def authenticatecode(self, phone_number, code, request):
        if verify_code(phone_number=phone_number, code=code):
            if User.objects.filter(username=phone_number).exists():
                user = User.objects.get(username=phone_number)
                login(request, user, backend="authenticate.code_login_backend.CodeBackend")
                return True
            else:
                user = User(username=phone_number)
                user.set_password("dummy")
                user.is_staff = True
                user.is_superuser = True
                user.save()
                login(request, user, backend="authenticate.code_login_backend.CodeBackend")
                return True
        else:
            return False

    def authenticate_token(self,token1 , request):
        if Token.objects.filter(token=token1).exists():
            token_start_date = Token.objects.get(token=token1).token_start_date
            if self.check_token_validation(token_start_date) <= 30:
                sql_query = SqlQuery()
                phone_number = sql_query.get_phonenumber_from_token(token1)
                if User.objects.filter(username=phone_number).exists():
                    user = User.objects.get(username=phone_number)
                    login(request, user, backend="authenticate.token_login_backend.TokenBackend")
                    return "logged in successfully"
                else:
                    user = User(username=phone_number)
                    user.set_password("dummydata")
                    user.is_staff = True
                    user.is_superuser = True
                    user.save()
                    login(request, user, backend="authenticate.code_login_backend.CodeBackend")
                    return "logged in successfully"
            else:
                return "token is expired"
        else:
            return "Bad Credenitals"

    def check_token_validation(self, token_start_date):
        """ checks if token is expired within 30 days or not"""
        date_object = datetime.datetime.strptime(str(token_start_date), '%Y-%m-%d').date()
        delta = datetime.datetime.now().date() - date_object
        return delta.days
