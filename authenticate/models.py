from django.db import models


# Create your models here.
class CustomUser(models.Model):
    """
    Custom User Entity IN Database
    """
    phone_number = models.CharField(max_length=200, unique=True)


class Token(models.Model):
    """
    Token Entity In Database
    """
    token = models.CharField(max_length=2000, unique=True, )
    token_start_date = models.DateField("token_start_date")
    phone_number = models.ForeignKey(CustomUser, on_delete=models.CASCADE, )
