from django.urls import path

from . import views

urlpatterns = [
    path('sendcode', views.sendcode, name='sendphonenumber'),
    path('codelogin' , views.loginbycode , name="loginbycode" ),
    path('tokenlogin' , views.loginbytoken , name = "loginbytoken"),
]