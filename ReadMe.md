# Phone Number Verification

this django project is a ready-to use phone number verification and token varification ,
with custom Authentication Backends Without Django REST Framework.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

These are Presqusites for Running Project Fluently
```
django = 2.2.0
SQlite = 3 (is built-in in django)
```


## Running the tests

4 End To End unit tests are implemeneted

### Break down into end to end tests

for running unit tests simply get inside projects root directory and run:

```
python mange.py test authentication.tests
```


## Deployment

just becuase it was a mock project i did'nt make a virtual enviormnet you can run it inside root of the project by:

```
python manage.py runserver
```

in production Mode Set Debug = False in Settings.py
